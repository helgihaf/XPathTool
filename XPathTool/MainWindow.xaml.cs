﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

namespace XPathTool
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private const string namespacePrefixBase = "ns";

		private Lazy<OpenUrlDialog> openUrlDialog = new Lazy<OpenUrlDialog>();
		private XDocument xdocument;
		private NamespaceContainer namespaces;

		public MainWindow()
		{
			InitializeComponent();
		}

		
		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
		}

		private void textBoxXml_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (textBoxXml != null)
			{
				UpdateXDocument();
			}
		}

		private void textBoxXPath_TextChanged(object sender, TextChangedEventArgs e)
		{
			UpdateQueryResult();
		}

		private void UpdateXDocument()
		{
			xdocument = null;
			namespaces = null;
			Brush textBrush;
			try
			{
				xdocument = XDocument.Parse(textBoxXml.Text);
			}
			catch (XmlException)
			{
			}

			if (xdocument != null)
			{
				DetectNamespaces(xdocument);
				textBrush = Brushes.Black;
			}
			else
			{
				textBrush = Brushes.Red;
			}
			textBoxXml.Foreground = textBrush;
		}

		private void UpdateQueryResult()
		{
			if (textBoxXPath != null && textBoxXml != null)
			{
				int resultCount = 0;
				string resultXml = "- no result -";

				if (textBoxXPath.Text.Length > 0 && xdocument != null)
				{
					var namespaceManager = GetNamespaceManager(xdocument);

					List<XElement> elements = null;
					try
					{
						elements = xdocument.XPathSelectElements(textBoxXPath.Text, namespaceManager).ToList();
					}
					catch (Exception ex)
					{
						textBoxResult.Text = ExceptionToString(ex);
					}
					if (elements != null)
					{
						var sb = new StringBuilder();
						foreach (var el in elements)
						{
							sb.AppendLine(el.ToString());
						}
						resultXml = sb.ToString();
						resultCount = elements.Count;
					}
				}
				textBoxResult.Text = resultXml;
				labelElementCount.Content = resultCount;
			}
		}

		private IXmlNamespaceResolver GetNamespaceManager(XDocument xdoc)
		{
			var reader = xdoc.CreateReader();
			var namespaceManager = new XmlNamespaceManager(reader.NameTable);
			if (namespaces != null)
			{
				foreach (var entry in namespaces.GetNamespaces(namespacePrefixBase))
				{
					namespaceManager.AddNamespace(entry.Key, entry.Value);
				}
			}
			return namespaceManager;
		}

		private static string ExceptionToString(Exception ex)
		{
			StringBuilder sb = new StringBuilder();

			int level = 0;

			while (ex != null)
			{
				sb.AppendLine("--Level " + level.ToString() + "--");
				sb.AppendLine("Type: " + ex.GetType().ToString());
				sb.AppendLine("Message: " + ex.Message);
				sb.AppendLine("Source: " + ex.Source);
				sb.AppendLine("StackTrace:" + ex.StackTrace);
				level++;
				ex = ex.InnerException;
			}

			return sb.ToString();
		}


		private void DetectNamespaces(XDocument xdoc)
		{
			namespaces = new NamespaceContainer();
			var navigator = xdoc.CreateNavigator();
			while (navigator.MoveToFollowing(XPathNodeType.Element))
			{
				var scopeNamespaces = navigator.GetNamespacesInScope(XmlNamespaceScope.All);
				if (scopeNamespaces.Count > 0)
				{
					namespaces.MergeFrom(scopeNamespaces);
				}
			}

			if (!namespaces.IsEmpty)
			{
				var sb = new StringBuilder();
				foreach (var entry in namespaces.GetNamespaces(namespacePrefixBase))
				{
					sb.AppendLine(entry.Key + " = " + entry.Value);
				}
				textBlockNamespaces.Text = sb.ToString();
			}
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			if (openUrlDialog.Value.ShowDialog() == true)
			{
				LoadXml(openUrlDialog.Value.Url);
			}
		}

		private void LoadXml(string url)
		{
			var uri = new Uri(url);
			if (uri.IsFile)
			{
				textBoxXml.Text = File.ReadAllText(url);
			}
			else
			{
				textBoxXml.Text = DownloadHtml(url);
			}
		}

		private string DownloadHtml(string url)
		{
			using (var client = new System.Net.WebClient())
			{
				return client.DownloadString(url);
			}
		}
	}
}
