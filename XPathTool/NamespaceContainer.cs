﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XPathTool
{
    internal class NamespaceContainer
    {
        private readonly HashSet<string> namespaces = new HashSet<string>();

        public bool IsEmpty
        {
            get { return namespaces.Count == 0; }
        }

        public void MergeFrom(IDictionary<string, string> scopeNamespaces)
        {
            foreach (var entry in scopeNamespaces)
            {
                namespaces.Add(entry.Value);
            }
        }

        public IDictionary<string, string> GetNamespaces(string prefixBase)
        {
            int counter = 1;
            var dictionary = new Dictionary<string, string>();
            foreach (var ns in namespaces)
            {
                dictionary.Add(prefixBase + counter++, ns);
            }
            return dictionary;
        }

    }
}
