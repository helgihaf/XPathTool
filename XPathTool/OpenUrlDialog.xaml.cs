﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace XPathTool
{
	/// <summary>
	/// Interaction logic for OpenUrlDialog.xaml
	/// </summary>
	public partial class OpenUrlDialog : Window
	{
		public OpenUrlDialog()
		{
			InitializeComponent();
			DataContext = this;
		}

		public string Url { get; set; }

		private void button_Click(object sender, RoutedEventArgs e)
		{
			OpenFileDialog openFileDialog = new OpenFileDialog();
			openFileDialog.Multiselect = false;
			openFileDialog.Filter = "XML files (*.xml)|*.xml|HTML files (*.htm*)|*.htm*|All files (*.*)|*.*";
			openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
			openFileDialog.ShowDialog();
		}

		private void button1_Click(object sender, RoutedEventArgs e)
		{
			DialogResult = true;
		}
	}
}
